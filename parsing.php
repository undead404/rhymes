<?php
/**
 * Class and Function List:
 * Function list:
 * - constructAccSelArea()
 * Classes list:
 */
define('PUBLIC', true);
if (!array_key_exists('text', $_POST)) die();
header('Content-type: text/html; charset=utf-8');
setlocale(LC_ALL, 'uk_UA');
mb_internal_encoding('UTF-8');

include_once ('classes/say.class.php');

function constructAccSelArea($wordsArray) 
{
    $selArea = '';
    foreach ($wordsArray as $key => $value) 
    {
        if (!Say::doStringHaveAVowel($value)) continue;
        $selArea.= '<div class="btn-group word wactive" data-id="' . ($key + 1) . '" role="group">';
        
        $letters = mbStringToArray($value);
        foreach ($letters as $key => $letter) 
        {
            $selArea.= '<button type="button" class="btn btn-default letter" data-pos="' . ($key + 1) . '">' . $letter . '</button>';
        }
        $selArea.= '</div>';
    }
    return $selArea;
}

$wordsArray = Say::getWordStringsFromString($_POST['text']);
$selArea = constructAccSelArea($wordsArray);

include ('html/parsing.html');
?>