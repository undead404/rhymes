"use strict";

function hasVowel(wordString)
{
    var ltrs = wordString.split('');
    for (var key in ltrs)
    {
        if (isVowel(ltrs[key]))
        {
            return true;
        }
    }
    return false;
}

function isVowel(letter)
{
    if (/^[аАеЕєЄїЇуУоОіІиИяЯюЮ]$/.test(letter)) return true;
    else return false;
}

function searchOneVowel(wordElement)
{
    var $element = $(wordElement);
    var vowelElement = null;
    var vowelsNum = 0;
    $element.children('.letter').each(function (index, element)
    {
        if (isVowel($(element).text()))
        {
            vowelsNum++;
            if (vowelElement == null)
            {
                vowelElement = element;
            }
        }
    });
    if (vowelsNum > 1) return;
    if (vowelElement != null)
    {
        setAccent(vowelElement);
    }
}

function searchOneVoweled()
{
    $('.word').each(function (index, element)
    {
        searchOneVowel(element);
    });
}

function saveWords()
{
    window.data = [];
    $('.word.wactive').each(function (index, element)
    {
        var $element = $(element);
        var wordId = $element.data('id');
        if (typeof window.accents[wordId] != 'undefined')
        {
            window.accents[wordId].forEach(function (element, index)
            {
                window.data.push(
                {
                    word: $element.text(),
                    accent: element.pos
                });
            });
        }
    });
}

function sendWords()
{
    if (typeof window.data == 'undefined') return;
    $.ajax(
    {
        cache: false,
        data:
        {
            words: window.data
        },
        dataType: 'text',
        error: function (data, textStatus, error)
        {
            alert('sending words:' + textStatus);
        },
        processData: true,
        success: function (data, textStatus)
        {
            window.response = data;
            console.log(data);
            alert('Записано!');
            window.location = '//' + location.host + '/rhymes/adding.php';
        },
        type: 'POST',
        url: '//' + location.host + '/rhymes/scripts/record.php'
    });
}

function setAccent(letterElem)
{
    var $letterElem = $(letterElem);
    if (!$letterElem.hasClass('letter') || !isVowel($letterElem.text())) return;
    var wordId = $letterElem.parent().data('id');
    if (typeof window.accents[wordId] != 'undefined')
    {
        window.accents[wordId].push(
        {
            elem: letterElem,
            pos: $letterElem.data('pos')
        });
    }
    else
    {
        window.accents[wordId] = [
        {
            elem: letterElem,
            pos: $letterElem.data('pos')
        }];
    }
    $letterElem.addClass('accent');
}

function unsetAccent(letterElem)
{
    var $letterElem = $(letterElem);
    if (!$letterElem.hasClass('letter')) return;
    var wordId = $letterElem.parent().data('id');
    if (typeof window.accents[wordId] == 'undefined') return;
    var letterPos = $letterElem.data('pos');
    window.accents[wordId].forEach(function (element, index)
    {
        if (element.pos == letterPos)
        {
            delete window.accents[wordId][index];
        }
    });
    $letterElem.removeClass('accent');
}
window.accents = [];
$(function ()
{
    $('.word').click(function (event)
    {
        var $target = $(event.target);
        if (!$target.hasClass('letter'))
        {
            $target.toggleClass('wactive');
        }
    });
    $('.letter').click(function (event)
    {
        if ($(event.target).hasClass('accent'))
        {
            unsetAccent(event.target);
        }
        else
        {
            setAccent(event.target);
        }
    });
    $('#sendBtn').click(function (event)
    {
        saveWords();
        sendWords();
    });
    $('.letter').each(function (index, element)
    {
        var $element = $(element);
        if (!isVowel($element.text()))
        {
            $element.addClass('disabled');
        }
    });
    searchOneVoweled();
});