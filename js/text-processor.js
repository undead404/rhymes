//var wordStrings=<?=json_encode($wordStrings);?>;
var curWordStringNum = 0;
//var accents=[];
/**
 * array with all the words already processed
 * @type {Array}
 */
var words = [];
/*for(var key in wordStrings)
{
    var val=wordStrings[key];
    accents[val]=0;
}*/
var currentWord;
/**
 * adding a word into the array
 * @param {string} wordString
 * @param {integer} accent
 */
function addAWord(wordString, accent)
{
    words.push(
    {
        'word': wordString,
        'accent': accent
    });
}
/**
 * get POST query string from window.words array
 * @return {[type]}
 */
function getQueryFromWordsArray()
{
    return JSON.stringify(words);
}
/**
 * getting number of vowel letters in a word
 * @param  {string} wordString
 * @return {integer}
 */
function getVowelNum(wordString)
{
    var vowelNum = 0;
    var ltrs = wordString.split('');
    for (var key in ltrs)
    {
        if (isVowel(ltrs[key]))
        {
            vowelNum++;
        }
    }
    return vowelNum;
}
/**
 * knowing whether a word have a vowel letter or not
 * @param  {string}  wordString
 * @return {Boolean}
 */
function hasVowel(wordString)
{
    var ltrs = wordString.split('');
    for (var key in ltrs)
    {
        if (isVowel(ltrs[key]))
        {
            return true;
        }
    }
    return false;
}
/**
 * knowing whether a letter is vowel or not
 * @param  {string}  letter
 * @return {Boolean}
 */
function isVowel(letter)
{
    if (/^[аАеЕєЄїЇуУоОіІиИяЯюЮ]$/.test(letter)) return true;
    else return false;
}
/**
 * saving a word with its accent
 */
function saveAccent()
{
    window.wordProc.find('.accent').each(function (index, elem)
    {
        addAWord(window.wordProc.text(), $(elem).data('x'));
        var ex = currentWord;
        showAWordElem(currentWord.next());
        $('#words-ready').append($(ex).detach().removeClass('edited').addClass('ready'));
    });
}
/**
 * sending words with AJAX.
 */
function sendWords()
{
    $.ajax(
    {
        url: 'words-saver.php',
        type: 'POST',
        dataType: 'text',
        data: 'words=' + encodeURIComponent(getQueryFromWordsArray(words)),
        //data: words,
        success: function (data, textStatus)
        {
            console.log(data);
        },
    });
}
/**
 * showing an element to edit a word
 * @param  {element} jElem
 */
function showAWordElem(jElem)
{
    if (currentWord)
    {
        currentWord.removeClass('edited');
    }
    currentWord = $(jElem);
    var wordStringChars = currentWord.text().split('');
    var wordProcessorContent = '';
    for (var key in wordStringChars)
    {
        var val = wordStringChars[key];
        wordProcessorContent += '<div class="letter" data-x="' + (wordStringChars.length - key) + '">' + val + '</div>';
    }
    window.wordProc.html(wordProcessorContent);
    if (window.wordProc.find('.accent').size() > 0)
    {
        window.saveButton.removeAttr('disabled');
    }
    else
    {
        window.saveButton.attr('disabled', 'disabled');
    }
    currentWord.addClass('edited');
    if (getVowelNum(window.wordProc.text()) < 2)
    {
        window.wordProc.find('.letter').each(function (index, elem)
        {
            if (isVowel($(elem).text()))
            {
                $(elem).click();
                window.saveButton.click();
            }
        });
    }
}
/*function showAWordString(wordString)
{
    var wordStringChars=wordString.split('');
    window.wordProc.html('');
    for(var key in wordStringChars)
    {
        var val=wordStringChars[key];
        window.wordProc.append('<div class="letter" data-x="'+(wordStringChars.length-key)+'">'+val+'</div>');
    }
    if (window.wordProc.find('.accent').size()>0) 
    {
        $('#savebutton').removeAttr('disabled');
    }
    else
    {
        $('#savebutton').attr('disabled','disabled');
    }
}*/
$(function ()
{
    window.wordProc = $('#word-processing');
    window.saveButton = $('#savebutton');
    showAWordElem($('#words-container').find(':first-child'));
    window.wordProc.on('click', '.letter', function (event)
    {
        if (!isVowel($(this).text()))
        {
            event.stopPropagation();
            return;
        }
        $(this).toggleClass('accent');
    });
    window.wordProc.click(function ()
    {
        if (window.wordProc.find('.accent').size() > 0)
        {
            window.saveButton.removeAttr('disabled');
        }
        else
        {
            window.saveButton.attr('disabled', 'disabled');
        }
    });
    $('#words-container .word').each(function (index, elem)
    {
        if (!hasVowel($(elem).text()))
        {
            $(elem).remove();
        }
    });
    $('#words-container').click(function (event)
    {
        showAWordElem(event.target);
    });
    $('#savebutton').click(function (event)
    {
        saveAccent();
    });
    $('#sendbutton').click(function (event)
    {
        sendWords();
    });
});