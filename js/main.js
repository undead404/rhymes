"use strict";
window.$accent = null;

function getAccentPos()
{
    if (window.$accent)
    {
        return window.$accent.data('pos');
    }
    else return -1;
}

function getLastVowelPos(wordString)
{
    var vowelNum = 0;
    var ltrs = wordString.split('');
    for (var i = ltrs.length - 1; i >= 0; i--)
    {
        if (isVowel(ltrs[i]))
        {
            return i + 1;
        }
    }
    return 0;
}
/**
 * getting number of vowel letters
 * @param  string wordString [description]
 * @return integer            [description]
 */
function getVowelNum(wordString)
{
    var vowelNum = 0;
    var ltrs = wordString.split('');
    for (var key in ltrs)
    {
        if (isVowel(ltrs[key]))
        {
            vowelNum++;
        }
    }
    return vowelNum;
}
/**
 * is a letter vowel?
 * @param  string  letter [description]
 * @return {Boolean}        [description]
 */
function isVowel(letter)
{
    if (/^[аАеЕєЄїЇуУоОіІиИяЯюЮ]$/.test(letter)) return true;
    else return false;
}

function loadMutations()
{
    window.$rhymesList.text('');
    window.$rhymesView.hide();
    window.wordString = $('#say-input').val();
    if (getAccentPos() < 1) return;
    $.ajax(
    {
        cache: false,
        dataType: 'json',
        error: function (data, textStatus, error)
        {
            alert('loading mutations: ' + textStatus);
        },
        processData: false,
        success: function (data, textStatus)
        {
            window.mutations = data;
            searchRhymes();
        },
        type: 'GET',
        url: '//' + location.host + '/rhymes/scripts/get-mutations.php?word=' + encodeURIComponent(window.wordString) + '&accent=' + getAccentPos()
    });
}

function constructAccSel(word)
{
    var wordArray = word.split('');
    var letterbuttons = '';
    wordArray.forEach(function (element, index)
    {
        letterbuttons += '<button aria-group="word" type="button" class="btn btn-lg btn-default letter" data-pos="' + (index + 1) + '">' + element + '</button>';
    });
    window.$accSelContainer.html(letterbuttons);
    window.$accSelSection.show();
    window.$accSelLink.click();
}

function reset()
{
    window.$accent = null;
    window.$rhymesList.text('');
    window.$rhymesView.hide();
    window.$accSelContainer.text('');
    window.$accSelSection.hide();
    window.$wordInput.val('');
    window.$submitWordButton.removeClass('disabled');
}

function searchRhymes()
{
    $.ajax(
    {
        cache: false,
        data:
        {
            hasending: (getAccentPos() != getLastVowelPos(window.wordString)),
            mutations: encodeURIComponent(window.mutations)
        },
        dataType: 'json',
        error: function (data, textStatus, error)
        {
            alert('searching rhymes:' + textStatus);
        },
        processData: true,
        success: function (data, textStatus)
        {
            window.rhymes = data;
            showRhymes();
        },
        type: 'POST',
        url: '//' + location.host + '/rhymes/scripts/get-rhymes.php'
    });
}

function setAccent(element)
{
    if (window.$accent != null) window.$accent.removeClass('accent');
    window.$accent = $(element);
    window.$accent.addClass('accent');
    window.$searchButton.removeClass('disabled');
}

function showRhymes()
{
    window.$RVHead.text('Рими до слова "' + window.wordString + '"');
    var rhymesString = '';
    if (window.rhymes.length < 1 || (window.rhymes.length == 1 && window.rhymes[0] == window.wordString))
    {
        rhymesString = '<li class="list-group-item list-group-item-danger">Вибачте. Рими не знайдені.</li>';
    }
    else
    {
        window.rhymes.forEach(function (element, index)
        {
            if (element != window.wordString)
            {
                rhymesString += '<li class="list-group-item list-group-item-success">' + element + '</li>';
            }
        });
    }
    window.$rhymesList.html(rhymesString);
    window.$rhymesView.show();
    window.$RVLink.click();
    window.$submitWordButton.removeClass('disabled');
}
$(function ()
{
    window.$accSelSection = $('#acc-sel');
    window.$accSelContainer = window.$accSelSection.find('#acc-sel-container');
    window.$wordInput = $('#say-input');
    window.$resetWordButton = $('#reset-word');
    window.$submitWordButton = $('#submit-word');
    window.$resetButton = $('#resetBtn');
    window.$searchButton = $('#searchBtn');
    window.$accSelLink = $('#accsellink');
    window.$rhymesView = $('#rhymes-view');
    window.$rhymesList = window.$rhymesView.find('#rhymes-list');
    window.$RVHead = window.$rhymesView.find('#rv-head');
    window.$RVLink = $('#RVlink');
    window.$resetWordButton.click(function (event)
    {
        reset();
    });
    window.$submitWordButton.click(function (event)
    {
        window.$submitWordButton.addClass('disabled');
        constructAccSel(window.$wordInput.val());
    });
    window.$accSelContainer.on('click', '.letter', function (event)
    {
        if (!isVowel($(event.target).text()))
        {
            event.stopPropagation();
            return;
        }
        setAccent(event.target);
    });
    window.$resetButton.click(function ()
    {
        reset();
        $('#toplink').click();
    });
    window.$searchButton.click(function ()
    {
        loadMutations();
    });
    window.$wordInput.keyup(function (event)
    {
        if (event.keyCode == 13)
        {
            window.$submitWordButton.click();
        }
    });
    window.$accSelContainer.keyup(function (event)
    {
        if (event.keyCode == 13 && window.$accent !== null)
        {
            window.$searchButton.click();
        }
    });
    /*$(document).keyup(function (event)
    {
        if (event.keyCode > 48 && event.keyCode < 58)
        {
            window.$accSelContainer.find('.letter[data-pos]=' + (event.keyCode - 48)).click();
        }
    });*/
})