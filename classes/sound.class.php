<?php
/**
 * Class and Function List:
 * Function list:
 * - addNext()
 * - isConsonant()
 * - isSingleLetter()
 * - isALetter()
 * - isLetterConsonant()
 * - isLetterIotated()
 * - isLetterVowel()
 * - isVowel()
 * - toggleSoft()
 * - unIotate()
 * - __clone()
 * - __construct()
 * - __get()
 * - __toString()
 * Classes list:
 * - Sound
 */
if (!defined("PUBLIC")) die();

setlocale(LC_ALL, 'uk_UA');
mb_internal_encoding('UTF-8');

class Sound
{
    
    /**
     * is the sound accented or not
     * @var boolean
     */
    public $isAccented = false;
    
    /**
     * is the sound soft or not
     * @var boolean
     */
    public $isSoft = false;
    
    /**
     * the letter itself
     * @var string
     */
    protected $letter;
    
    /**
     * next node in the list
     * @var Sound
     */
    public $next;
    
    /**
     * previous node in the list
     * @var Sound
     */
    public $prev;
    
    /**
     * Matrix of sound transitions. Initiated at the bottom of this file
     * @var Array
     */
    public static $transMatrix;
    
    /**
     * relevance multiplyer for the sound being softened
     * @var float
     */
    public static $transSoftRel = 0.7;
    
    /**
     * adding a sound as the next node
     * @param string|Sound  $letter     [description]
     * @param boolean $isSoft     [description]
     * @param boolean $isAccented [description]
     */
    public function addNext($letter, $isSoft = false, $isAccented = false) 
    {
        if ($letter instanceof Sound) 
        {
            $this->next = $letter;
            $this->next->prev = $this;
        } else
        {
            $this->next = new Sound($letter, $isSoft, $isAccented);
            $this->next->prev = $this;
        }
    }
    
    /**
     * is the sound consonant
     * @return boolean [description]
     */
    public function isConsonant() 
    {
        return self::isLetterConsonant($this->letter);
    }
    
    public static function isSingleLetter($symbol) 
    {
        
        //return preg_match(self::$RXSingleLetter, $symbol);
        switch ($symbol) 
        {
        case 'а':
        case 'А':
        case 'б':
        case 'Б':
        case 'в':
        case 'В':
        case 'г':
        case 'Г':
        case 'ґ':
        case 'Ґ':
        case 'д':
        case 'Д':
        case 'дж':
        case 'Дж':
        case 'ДЖ':
        case 'дз':
        case 'Дз':
        case 'ДЗ':
        case 'е':
        case 'Е':
        case 'ж':
        case 'Ж':
        case 'з':
        case 'З':
        case 'и':
        case 'И':
        case 'і':
        case 'І':
        case 'й':
        case 'Й':
        case 'к':
        case 'К':
        case 'л':
        case 'Л':
        case 'м':
        case 'М':
        case 'н':
        case 'Н':
        case 'о':
        case 'О':
        case 'п':
        case 'П':
        case 'р':
        case 'Р':
        case 'с':
        case 'С':
        case 'т':
        case 'Т':
        case 'у':
        case 'У':
        case 'ф':
        case 'Ф':
        case 'х':
        case 'Х':
        case 'ц':
        case 'Ц':
        case 'ч':
        case 'Ч':
        case 'ш':
        case 'Ш':
            return true;
            break;

        default:
            return false;
            break;
        }
    }
    
    /**
     * is string a single letter?
     * @param  string  $symbol [description]
     * @return boolean         [description]
     */
    public static function isALetter($symbol) 
    {
        
        //return preg_match(self::$RXALetter, $symbol);
        switch ($symbol) 
        {
        case 'а':
        case 'А':
        case 'б':
        case 'Б':
        case 'в':
        case 'В':
        case 'г':
        case 'Г':
        case 'ґ':
        case 'Ґ':
        case 'д':
        case 'Д':
        case 'е':
        case 'Е':
        case 'є':
        case 'Є':
        case 'ж':
        case 'Ж':
        case 'з':
        case 'З':
        case 'и':
        case 'И':
        case 'і':
        case 'І':
        case 'ї':
        case 'Ї':
        case 'й':
        case 'Й':
        case 'к':
        case 'К':
        case 'л':
        case 'Л':
        case 'м':
        case 'М':
        case 'н':
        case 'Н':
        case 'о':
        case 'О':
        case 'п':
        case 'П':
        case 'р':
        case 'Р':
        case 'с':
        case 'С':
        case 'т':
        case 'Т':
        case 'у':
        case 'У':
        case 'ф':
        case 'Ф':
        case 'х':
        case 'Х':
        case 'ц':
        case 'Ц':
        case 'ч':
        case 'Ч':
        case 'ш':
        case 'Ш':
        case 'щ':
        case 'Щ':
        case 'ь':
        case 'Ь':
        case 'ю':
        case 'Ю':
        case 'я':
        case 'Я':
            return true;
            break;

        default:
            return false;
            break;
        }
    }
    
    /**
     * is a letter consonant?
     * @param  string  $letter [description]
     * @return boolean         [description]
     */
    public static function isLetterConsonant($letter) 
    {
        
        //return preg_match(self::$RXCons, $letter);
        switch ($letter) 
        {
        case 'б':
        case 'Б':
        case 'в':
        case 'В':
        case 'г':
        case 'Г':
        case 'ґ':
        case 'Ґ':
        case 'д':
        case 'Д':
        case 'ж':
        case 'Ж':
        case 'з':
        case 'З':
        case 'й':
        case 'Й':
        case 'к':
        case 'К':
        case 'л':
        case 'Л':
        case 'м':
        case 'М':
        case 'н':
        case 'Н':
        case 'п':
        case 'П':
        case 'р':
        case 'Р':
        case 'с':
        case 'С':
        case 'т':
        case 'Т':
        case 'ф':
        case 'Ф':
        case 'х':
        case 'Х':
        case 'ц':
        case 'Ц':
        case 'ч':
        case 'Ч':
        case 'ш':
        case 'Ш':
        case 'щ':
        case 'Щ':
            return true;
            break;

        default:
            return false;
            break;
        }
    }
    
    /**
     * is a letter iotated?
     * @param  string  $letter
     * @return boolean
     */
    public static function isLetterIotated($letter) 
    {
        
        //return preg_match(self::$RXIotated, $letter);
        switch ($letter) 
        {
        case 'я':
        case 'Я':
        case 'ю':
        case 'Ю':
        case 'є':
        case 'Є':
        case 'ї':
        case 'Ї':
            return true;
            break;

        default:
            return false;
            break;
        }
    }
    
    /**
     * is a letter vowel?
     * @param  string  $letter
     * @return boolean
     */
    public static function isLetterVowel($letter) 
    {
        
        //return preg_match(self::$RXVowel, $letter);
        switch ($letter) 
        {
        case 'а':
        case 'А':
        case 'е':
        case 'Е':
        case 'є':
        case 'Є':
        case 'и':
        case 'И':
        case 'і':
        case 'І':
        case 'ї':
        case 'Ї':
        case 'о':
        case 'О':
        case 'у':
        case 'У':
        case 'ю':
        case 'Ю':
        case 'я':
        case 'Я':
            return true;
            break;

        default:
            return false;
            break;
        }
    }
    
    /**
     * is the sound vowel?
     * @return boolean [description]
     */
    public function isVowel() 
    {
        return self::isLetterVowel($this->letter);
    }
    
    /**
     * softening non-soft sound or unsoften soft one
     */
    public function toggleSoft() 
    {
        $this->isSoft = !$this->isSoft;
    }
    
    /**
     * uniotating letter
     * @param  string $letter [description]
     * @return string         [description]
     */
    public static function unIotate($letter) 
    {
        switch ($letter) 
        {
        case 'я':
            return 'а';
            break;

        case 'ю':
            return 'у';
            break;

        case 'є':
            return 'е';
            break;

        case 'ї':
            return 'і';
            break;

        default:
            return '';
            break;
        }
    }
    public function __clone() 
    {
        $this->prev = null;
        $this->next = null;
    }
    public function __construct($sound, $isSoft = false, $isAccented = false) 
    {
        if (self::isSingleLetter($sound) || $sound == 'дз' || $sound == 'дж') $this->letter = $sound;
        else throw new Exception("Error Processing Request", 1);
        if ($isSoft) $this->isSoft = true;
        elseif ($sound == 'й') $this->isSoft = true;
        if ($isAccented) $this->isAccented = true;
    }
    function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
            return $this->$property;
        }
    }
    public function __toString() 
    {
        if ($this->isSoft && $this->isConsonant() && $this->letter != 'й') return $this->letter . 'ь';
        else return $this->letter;
    }
    
    /*public function __destruct()
    {
    if ($this->prev) 
    {
        unset($this->prev->next);
        unset($this->prev);
    }
    if ($this->next) 
    {
        unset($this->next->prev);
        unset($this->next);
    }
    }*/
}
Sound::$transMatrix = include ('transmatrix.sound.php');
?>