<?php
/**
 * Class and Function List:
 * Function list:
 * - addMutation()
 * - mutate()
 * - searchSimMutationPos()
 * - sortMutations()
 * - __construct()
 * - __get()
 * - __destruct()
 * Classes list:
 * - Raid
 */
if (!defined("PUBLIC")) die();
setlocale(LC_ALL, 'uk_UA');
mb_internal_encoding('UTF-8');

include_once ('mutation.class.php');

class Raid
{
    
    /**
     * all the mutations approved
     * @var Array
     */
    protected $mutations = array();
    
    /**
     * all the mutations strings
     * @var Array
     */
    protected $mutationsStringsArray;
    
    /**
     * the say to be rhymed
     * @var Say
     */
    protected $rhymed;
    
    /**
     * the limit for mutations with less relevance not to be approved
     * @var float
     */
    public $relLimit = 0.5;
    
    /**
     * adding mutation to the array
     * @param Mutation $mutation [description]
     */
    public function addMutation($mutation) 
    {
        if (!$mutation) return;
        $simMutationPos = $this->searchSimMutationPos($mutation);
        if ($simMutationPos < 0) 
        {
            $this->mutations[] = $mutation;
            return true;
        } elseif ($this->mutations[$simMutationPos]->rel < $mutation->rel) 
        {
            $this->mutations[$simMutationPos] = $mutation;
            return true;
        }
        return false;
    }
    
    /**
     * starting mutation
     */
    protected function mutate() 
    {
        $this->mutations[0] = new Mutation($this->rhymed->crucial, 1.0, $this);
        $this->mutations[0]->mutate();
    }
    
    /**
     * searching mutation with the same pronunciation in the array
     * @param  Mutation $mutation [description]
     * @return integer           [description]
     */
    protected function searchSimMutationPos($mutation) 
    {
        $mutationString = $mutation->__toString();
        foreach ($this->mutations as $key => $value) 
        {
            if ($value->__toString() == $mutationString) 
            {
                return $key;
            }
        }
        return -1;
    }
    
    /**
     * sorting some array of mutations
     * @param  Array $mutations [description]
     * @return Array            [description]
     */
    protected static function sortMutations($mutations) 
    {
        $mutNum = count($mutations);
        if ($mutNum == 1) return $mutations;
        $pillar = (int)($mutNum / 2);
        $arrayLarger = array();
        $arrayEqual = array();
        $arrayLess = array();
        foreach ($mutations as $key => $mutation) 
        {
            if ($mutation->rel > $mutations[$pillar]->rel) $arrayLarger[] = $mutation;
            if ($mutation->rel == $mutations[$pillar]->rel) $arrayEqual[] = $mutation;
            if ($mutation->rel < $mutations[$pillar]->rel) $arrayLess[] = $mutation;
        }
        if (count($arrayLarger) > 1) $arrayLarger = self::sortMutations($arrayLarger);
        if (count($arrayLess) > 1) $arrayLess = self::sortMutations($arrayLess);
        return array_merge($arrayLarger, $arrayEqual, $arrayLess);
    }
    public function __construct($say) 
    {
        if (!$say) return;
        $this->rhymed = $say;
        $this->mutate();
        
        $this->mutations = self::sortMutations($this->mutations);
        
    }
    
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
            return $this->$property;
        } elseif ($property == 'mutationsStrings') 
        {
            $this->mutationsStringsArray = array();
            if ($this->mutations) 
            {
                foreach ($this->mutations as $key => $mutation) 
                {
                    $this->mutationsStringsArray[] = $mutation->__toString();
                }
            }
            return $this->mutationsStringsArray;
        }
    }
    public function __destruct() 
    {
        if ($this->mutationsStringsArray) unset($this->mutationsStringsArray);
        unset($this->mutations);
    }
}
?>