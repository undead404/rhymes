<?php
/**
 * Class and Function List:
 * Function list:
 * - addMutation()
 * - mutate()
 * - __construct()
 * - __toString()
 * Classes list:
 * - Mutation extends Say
 */
if (!defined("PUBLIC")) die();
setlocale(LC_ALL, 'uk_UA');
mb_internal_encoding('UTF-8');
include_once 'say.class.php';
include_once 'raid.class.php';

class Mutation extends Say
{
    
    /**
     * the raid associated
     * @var Raid
     */
    protected $raid;
    
    /**
     * relevance
     * @var float
     */
    public $rel = 1.0;
    
    /**
     * saving a mutation in the raid
     * @param Mutation $mutation [description]
     */
    protected function addMutation($mutation) 
    {
        return $this->raid->addMutation($mutation);
    }
    
    /**
     * creating child mutations
     */
    public function mutate() 
    {
        $soundsNum = $this->soundsNum;
        if ($soundsNum < 2) return;
        for ($i = 2, $current = $this->sounds->next; $current && $i <= $soundsNum; $i++, $current = $current->next) 
        {
            if ($current->isConsonant() && !($current->next && $current->next->letter == 'и')) 
            {
                $newRel = $this->rel * Sound::$transSoftRel;
                if ($newRel >= $this->raid->relLimit) 
                {
                    $newMutation = new Mutation($this, $newRel, $this->raid);
                    $toSoften = $newMutation->getSoundAt($i);
                    $toSoften->toggleSoft();
                    $added = $this->addMutation($newMutation);
                    if ($added) $newMutation->mutate();
                }
            }
            foreach (Sound::$transMatrix[$current->letter] as $key => $value) 
            {
                $newRel = ($this->rel) * $value;
                if ($newRel >= $this->raid->relLimit) 
                {
                    $newMutation = new Mutation($this, $newRel, $this->raid);
                    $newSound = new Sound($key, $current->isSoft);
                    $newMutation->setSoundAt($i, $newSound);
                    $added = $this->addMutation($newMutation);
                    if ($added) $newMutation->mutate();
                }
            }
        }
    }
    
    public function __construct($say, $rel = 1.0, $raid, $level = 0) 
    {
        $this->raid = $raid;
        if ($rel < $this->raid->relLimit) 
        {
            throw new Exception("Mutation not approved", 1);
        }
        parent::__construct($say);
        
        if ($say instanceof Mutation) 
        {
            $this->rel = ($say->rel < $rel) ? $say->rel : $rel;
        } else
        {
            $this->rel = $rel;
        }
    }
    public function __toString() 
    {
        return $this->getPronunciation();
    }
}
