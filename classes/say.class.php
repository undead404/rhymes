<?php
/**
 * Class and Function List:
 * Function list:
 * - mbStringToArray()
 * - addSound()
 * - doStringHaveAVowel()
 * - getWordStringsFromString()
 * - getLastVowel()
 * - getPronunciation()
 * - getSoundAt()
 * - getVowelNum()
 * - parseCrucial()
 * - parseSounds()
 * - saveInDb()
 * - setSoundAt()
 * - __construct()
 * - __get()
 * - __toString()
 * - __destruct()
 * Classes list:
 * - Say
 */
if (!defined("PUBLIC")) 
{
    die();
}
setlocale(LC_ALL, 'uk_UA');
mb_internal_encoding('UTF-8');
include_once ('sound.class.php');

/**
 * getting array of symbols from multibyte string
 * @param  string $string [description]
 * @return array         [description]
 */
function mbStringToArray($string) 
{
    $strlen = mb_strlen($string);
    $array = array();
    while ($strlen) 
    {
        $array[] = mb_substr($string, 0, 1);
        $string = mb_substr($string, 1, $strlen);
        $strlen = mb_strlen($string);
    }
    return $array;
}
class Say
{
    protected static $RXWord = '/[аАбБвВгГґҐдДеЕєЄжЖзЗиИіІїЇйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩьЬюЮяЯ-’\'`]+/';
    
    /**
     * the accented sound
     * @var Sound
     */
    protected $accent;
    
    /**
     * the number of the letter accented, 0 if undefined
     * @var integer
     */
    protected $accentLetterPos = 0;
    
    /**
     * the number of the sound accented, 0 if undefined
     * @var integer
     */
    protected $accentSoundPos = 0;
    
    /**
     * the entire say
     * @var string
     */
    protected $entire = '';
    
    /**
     * the main part of the say
     * @var string
     */
    protected $crucial = '';
    
    /**
     * the accented sound
     * @var Sound
     */
    protected $lastSound;
    
    /**
     * the ending part of the say
     * @var string
     */
    protected $ending = '';
    
    /**
     * list of sounds
     * @var Sound
     */
    protected $sounds;
    
    /**
     * the number of sounds
     * @var integer
     */
    protected $soundsNum = 0;
    
    /**
     * adding a sound to the sounds list.
     * @param [string|Sound]  $letter
     * @param boolean $isSoft
     * @param boolean $isAccented
     */
    protected function addSound($letter, $isSoft = false, $isAccented = false) 
    {
        
        //echo (__METHOD__ . '(' . $letter . ',' . (string)$isSoft . ',' . (string)$isAccented . ');<br/>');
        if ($letter instanceof Sound) $newSound = clone $letter;
        elseif (is_string($letter)) $newSound = new Sound($letter, $isSoft, $isAccented);
        else return;
        if ($this->sounds && $this->lastSound) 
        {
            $this->lastSound->next = $newSound;
            $newSound->prev = $this->lastSound;
            $newSound->next = null;
        } else
        {
            $this->sounds = $newSound;
            $newSound->prev = null;
            $newSound->next = null;
        }
        $this->lastSound = $newSound;
        $this->soundsNum++;
    }
    public static function doStringHaveAVowel($string) 
    {
        $lettersArray = mbStringToArray($string);
        foreach ($lettersArray as $letter) 
        {
            if (Sound::isLetterVowel($letter)) 
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * parsing says in strings from text
     * @param  string $text
     * @return Array
     */
    public static function getWordStringsFromString($text) 
    {
        $sayStrings = array();
        preg_match_all(self::$RXWord, $text, $sayStrings);
        foreach ($sayStrings[0] as $key => $sayString) 
        {
            $sayStrings[0][$key] = mb_strtolower($sayString, 'utf-8');
        }
        return $sayStrings[0];
    }
    
    /**
     * getting the last vowel letter in the say
     * @return Sound
     */
    protected function getLastVowel() 
    {
        
        //$currentPos = $this->soundsNum;
        $current = $this->lastSound;
        do 
        {
            if ($current->isVowel()) 
            {
                return $current;
            }
        }
        while ($current = $current->prev);
        return null;
    }
    
    /**
     * getting pronunciation of the say
     * @return string
     */
    public function getPronunciation() 
    {
        $pronunc = '';
        $current = $this->sounds;
        while ($current) 
        {
            $pronunc.= $current;
            $current = $current->next;
        }
        return $pronunc;
    }
    
    /**
     * getting a sound at a certain position
     * @param  integer $pos [description]
     * @return Sound      [description]
     */
    public function getSoundAt($pos) 
    {
        
        //echo 'getting sound #' . $pos . ' from "' . $this . '"<br/>';
        if ($pos < 1) return null;
        $current = $this->sounds;
        for ($i = 1; $i < $pos && $current; $i++) 
        {
            $current = $current->next;
        }
        
        //echo 'found sound ' . $current . '<br/>';
        return $current;
    }
    
    /**
     * getting number of vowel letters in a say
     * @return integer
     */
    public function getVowelNum() 
    {
        $vowelsNum = 0;
        $current = $this->sounds;
        while ($current) 
        {
            if ($current->isVowel()) 
            {
                $vowelsNum++;
            }
            $current = $current->next;
        }
        return $vowelsNum;
    }
    
    /**
     * finding crucial part of the word
     */
    protected function parseCrucial() 
    {
        if ($this->accentSoundPos < 1) 
        {
            return;
        }
        $crucialArray = array();
        $endingArray = array();
        $soundsNum = $this->soundsNum;
        $lastVowel = $this->getLastVowel();
        if ($lastVowel === $this->accent) 
        {
            for ($current = $this->accent; $current; $current = $current->next) 
            {
                $crucialArray[] = $current->__toString();
            }
        } else
        {
            for ($current = $this->accent; $current !== $lastVowel && $current; $current = $current->next) 
            {
                $crucialArray[] = $current->__toString();
            }
            for ($current = $lastVowel; $current; $current = $current->next) 
            {
                $endingArray[] = $current->__toString();
            }
        }
        $this->crucial = implode('', $crucialArray);
        unset($crucialArray);
        $this->ending = implode('', $endingArray);
        unset($endingArray);
    }
    
    /**
     * parsing sounds from letters
     */
    protected function parseSounds() 
    {
        $soundsNum = 0;
        $sayArray = mbStringToArray($this->entire);
        $sayLen = count($sayArray);
        $current = null;
        for ($i = 0; $i < $sayLen; $i++) 
        {
            
            //echo $sayArray[$i];
            if (Sound::isLetterIotated($sayArray[$i])) 
            {
                if ($soundsNum > 0 && $current->isConsonant() && Sound::isLetterConsonant($sayArray[$i - 1])) 
                {
                    $current->isSoft = true;
                } else
                {
                    if ($current) 
                    {
                        $current->addNext(new Sound('й'));
                        $soundsNum++;
                        $current = $current->next;
                    } else
                    {
                        $this->sounds = $current = new Sound('й');
                        $soundsNum = 1;
                    }
                }
                if ($current) 
                {
                    $current->addNext(new Sound(Sound::unIotate($sayArray[$i])));
                    $soundsNum++;
                    $current = $current->next;
                } else
                {
                    $this->sounds = $current = new Sound(Sound::unIotate($sayArray[$i]));
                    $soundsNum = 1;
                }
                if ($i + 1 == $this->accentLetterPos) 
                {
                    $current->isAccented = true;
                    $this->accent = $current;
                    $this->accentSoundPos = $soundsNum;
                }
            } elseif ($sayArray[$i] == '’') 
            {
            } elseif (Sound::isLetterConsonant($sayArray[$i])) 
            {
                if ($sayArray[$i] == 'щ') 
                {
                    if ($current) 
                    {
                        $current->addNext(new Sound('ш'));
                        $soundsNum++;
                        $current = $current->next;
                    } else
                    {
                        $this->sounds = $current = new Sound('ш');
                        $soundsNum = 1;
                    }
                    $current->addNext(new Sound('ч'));
                    $soundsNum++;
                    $current = $current->next;
                } elseif ($soundsNum > 0 && $sayArray[$i] == 'д' && array_key_exists($i + 1, $sayArray) && $sayArray[$i + 1] == 'з') 
                {
                    if ($current) 
                    {
                        $current->addNext(new Sound('дз'));
                    } else
                    {
                        $this->sounds = $current = new Sound('дз');
                        $soundsNum = 1;
                    }
                    $current = $current->next;
                    $soundsNum++;
                    $i++;
                } elseif ($soundsNum > 0 && $sayArray[$i] == 'д' && array_key_exists($i + 1, $sayArray) && $sayArray[$i + 1] == 'ж') 
                {
                    if ($current) 
                    {
                        $current->addNext(new Sound('дж'));
                    } else
                    {
                        $this->sounds = $current = new Sound('дж');
                        $soundsNum = 1;
                    }
                    $current = $current->next;
                    $soundsNum++;
                    $i++;
                } else
                {
                    if ($current) 
                    {
                        $current->addNext(new Sound($sayArray[$i]));
                        $soundsNum++;
                        $current = $current->next;
                    } else
                    {
                        $this->sounds = $current = new Sound($sayArray[$i]);
                        $soundsNum = 1;
                    }
                }
            } elseif ($sayArray[$i] == 'ь') 
            {
                $current->isSoft = true;
            } elseif (Sound::isLetterVowel($sayArray[$i])) 
            {
                if ($current) 
                {
                    $current->addNext(new Sound($sayArray[$i]));
                    $soundsNum++;
                    $current = $current->next;
                } else
                {
                    $this->sounds = $current = new Sound($sayArray[$i]);
                    $soundsNum = 1;
                }
                if ($i + 1 == $this->accentLetterPos) 
                {
                    $current->isAccented = true;
                    $this->accentSoundPos = $soundsNum;
                    $this->accent = $current;
                }
            }
        }
        if ($current) $this->lastSound = $current;
        $this->soundsNum = $soundsNum;
        
        //var_dump($this->soundsSet);
        unset($sayArray);
    }
    
    public function saveInDb($db) 
    {
        if (!$db->getRow('SELECT * FROM `words` WHERE `entire` = ?s AND `accent` = ?i', $this->entire, $this->accentLetterPos)) 
        {
            if ($this->ending == '') 
            {
                $db->query('INSERT INTO `words` (`entire` ,`body` ,`last` ,`accent`) VALUES (?s,?s,NULL,?i)', $this->entire, $this->crucial, $this->accentLetterPos);
            } else
            {
                $db->query('INSERT INTO `words` (`entire` ,`body` ,`last` ,`accent`) VALUES (?s,?s,?s,?i)', $this->entire, $this->crucial, $this->ending, $this->accentLetterPos);
            }
        }
    }
    
    /**
     * replacing sound at the position by the other one
     * @param integer $pos   [description]
     * @param Sound $sound [description]
     */
    public function setSoundAt($pos, $sound) 
    {
        
        //die('setSoundAt');
        if (!$sound) return;
        if ($pos < 1 || $pos > $this->soundsNum) return;
        $current = $this->getSoundAt($pos);
        if ($pos > 1) 
        {
            $sound->prev = $current->prev;
            $sound->prev->next = $sound;
        } else $sound->prev = null;
        if ($pos < $this->soundsNum) 
        {
            $sound->next = $current->next;
            $sound->next->prev = $sound;
        } else $sound->next = null;
        unset($current);
    }
    
    /**
     * Creates a Say object from a say in a string and (optionally) number of the accented letter.
     * @param string  $argument
     * @param integer $accentPos
     */
    function __construct($argument, $accentPos = 0) 
    {
        if ($argument instanceof Say) 
        {
            
            //die('ololo');
            $current = $argument->sounds;
            do 
            {
                $this->addSound($current);
            }
            while ($current = $current->next);
            $this->accentLetterPos = $argument->accentLetterPos;
            $this->accentSoundPos = $argument->accentSoundPos;
            $this->entire = $argument->entire;
        } elseif (!is_string($argument)) 
        {
            throw new Exception("Error Processing Request", 1);
        } else
        {
            $this->entire = mb_strtolower($argument);
            $this->accentLetterPos = $accentPos;
            $this->parseSounds();
        }
        if ($this->accentLetterPos > 0) 
        {
            $this->parseCrucial();
        }
    }
    
    /**
     * getting read-only fields
     * @param  string $property [description]
     * @return mixed           [description]
     */
    function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
            return $this->$property;
        }
    }
    
    /**
     * casting into string
     * @return string [description]
     */
    function __toString() 
    {
        
        /*if ($this->accentPos == 0)
        {
            return $this->entire;
        } else
        {
            return mb_substr($this->entire, 0, $this->accentPos) . html_entity_decode('&#769;') . mb_substr($this->entire, $this->accentPos, mb_strlen($this->entire) - $this->accentPos);
        }*/
        return $this->entire;
    }
    function __destruct() 
    {
        if ($this->sounds) 
        {
            for ($current = $this->lastSound; $current->prev; $current = $current->prev) 
            {
                unset($current->prev->next);
            }
        }
    }
}
