<?php
define('PUBLIC', TRUE);
include_once 'classes/word.php';

/**
 * words with accents in stdClass array
 * @var Array
 */
$wordsRaw = json_decode($_POST['words']);
$words = Array();
foreach ($wordsRaw as $key => $word) {
    $words[] = new Word($word->word, $word->accent);
}
echo $words[3]->accent;

//var_dump($words);

?>