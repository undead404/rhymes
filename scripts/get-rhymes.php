<?php
/**
 * Class and Function List:
 * Function list:
 * Classes list:
 */
define('PUBLIC', true);
header('Content-type: text/json; charset=utf-8');
setlocale(LC_ALL, 'uk_UA');
if (!array_key_exists('mutations', $_POST) || !array_key_exists('hasending', $_POST)) die();
$mutations = urldecode($_POST['mutations']);
$mutations = explode(',', $mutations);
$hasEnding = ($_POST['hasending'] == 'true') ? true : false;
include_once ('../classes/safemysql.class.php');

$db = new SafeMySQL();
$mutationsSet = '';
$flag = false;
foreach ($mutations as $mutation) 
{
    if ($flag) $mutationsSet.= ',';
    $mutationsSet.= $mutation;
    $flag = true;
}
if ($hasEnding) 
{
    $query = 'SELECT `entire` FROM `words` WHERE `body` IN (?a) AND `last` IS NOT NULL ORDER BY FIND_IN_SET(`body`,?s) ASC, CHAR_LENGTH(`entire`) DESC';
} else
{
    $query = 'SELECT `entire` FROM `words` WHERE `body` IN (?a) AND `last` IS NULL ORDER BY FIND_IN_SET(`body`,?s) ASC, CHAR_LENGTH(`entire`) DESC';
}
//$q = $db->parse($query, $mutations, $mutationsSet);
$rhymes = $db->getCol($query, $mutations, $mutationsSet);
//$rhymes[] = $q;
echo json_encode($rhymes);

//print_r($rhymes);


?>