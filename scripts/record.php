<?php
/**
 * Class and Function List:
 * Function list:
 * Classes list:
 */
define('PUBLIC', true);
header('Content-type: text/plain; charset=utf-8');
setlocale(LC_ALL, 'uk_UA');
include_once ('../classes/say.class.php');
include_once ('../classes/safemysql.class.php');
if (!array_key_exists('words', $_POST)) die();

$words = array();

$db = new SafeMySQL();
foreach ($_POST['words'] as $key => $word) 
{
    $words[$key] = new Say($word['word'], $word['accent']);
}
$db = new SafeMySQL();

foreach ($words as $key => $word) 
{
    $word->saveInDb($db);
}
?>