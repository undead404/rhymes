<?php
/**
* Class and Function List:
* Function list:
* - parseWords()
* Classes list:
*/
define('PUBLIC', true);
header('Content-type: text/html; charset=utf-8');
setlocale(LC_ALL, 'uk_UA');

/**
 * parsing says from text and adding one-vowel says to database
 * @param  String $text
 * @return Array
 */
function parseWords($text)
{
    $sayStrings = Say::getWordStringsFromString($text);
    $unprocWordStrs = [];
    $current = null;
    $vN = 0;
    foreach ($sayStrings as $key => $sayString) {
        $current = new Say($sayString);
        $vN = $current->getVowelNum();
        if ($vN == 0) continue;
        elseif ($vN == 1) $current->accent = $current->getFirstVowelNum();
    }
    return $sayStrings;
}

if (!$_POST['text']) die('ololo');

/**
 * the whole text from POST
 * @var String
 */
$text = $_POST['text'];
include_once 'classes/say.php';

/**
 * says in strings
 * @var Array
 */
$sayStrings = parseWords($text);
require ('html/text-processor.html');
?>